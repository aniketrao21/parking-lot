# Parking Lot CLI

## [Problem Statement](ParkingLot-1.3.1.pdf)

### How to execute

- For Interactive Mode

```sh
bin/setup; bin/parking_lot
```

-- Available Commands

```sh
create_parking_lot 6 # Creates a Parking lot with 6 spots
park KA-01-HH-1234 White # Parks a Vehicle
park KA-01-HH-9999 White
park KA-01-BB-0001 Black
park KA-01-HH-7777 Red
park KA-01-HH-2701 Blue
park KA-01-HH-3141 Black
leave 4 # Unparks a Vehicle
status # Gives Status of Parking Lot
park KA-01-P-333 White
park DL-12-AA-9999 White
registration_numbers_for_cars_with_colour White # This gives the registration numbers of cars by color

slot_numbers_for_cars_with_colour White # This gives the slot numbers of cars by color

slot_number_for_registration_number KA-01-HH-3141 # This gives the slot number of car by registration number
slot_number_for_registration_number MH-04-AY-1111
```

- For File Input Mode

```sh
bin/setup; bin/parking_lot path_to_your_text_file
```

### How to run tests

```sh
$ bundle exec rspec
```