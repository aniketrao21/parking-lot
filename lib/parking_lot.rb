require_relative 'libraries'
class ParkingLot
  attr_reader :spots

  def initialize(spot_count)
    raise ArgumentError if spot_count.nil?
    @spots = create_parking_spots(spot_count)
  end

  def size
    @spots.size
  end

  def can_park?
    @spots.map(&:vacant?).any?
  end

  def vacant_spots
    @spots.select(&:vacant?) - [nil]
  end

  def occupied_spots
    @spots.select(&:occupied?) - [nil]
  end

  def find_spots_with_color(color)
    occupied_spots.select do |spot|
      spot if spot.vehicle.color == color
    end
  end

  def find_vehicles_with_color(color)
    vehicles = []
    occupied_spots.each do |spot|
      vehicles << spot.vehicle if spot.vehicle.color == color
    end
    vehicles -= [nil]
  end

  def find_spot_with_reg_number(reg_number)
    parking_spot = occupied_spots.find do |spot|
      spot if spot.vehicle.reg_number == reg_number
    end
    parking_spot
  end

  def vacate_spot(serial_number)
    parking_spot = @spots.find { |spot| spot.serial_number == serial_number }
    parking_spot.vehicle = nil if parking_spot
    parking_spot
  end

  private

  def create_parking_spots(spot_count)
    spots = []
    spot_count.times do |index|
      spots << ParkingSpot.new(nil, (1..6).to_a.sample, nil, index + 1, nil)
    end
    spots
  end
end
