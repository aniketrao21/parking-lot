require_relative 'libraries'
class ParkingStructure
  attr_reader :parking_lots

  def initialize(parking_lots)
    @parking_lots = create_parking_lots(parking_lots)
  end

  private

  def create_parking_lots(parking_lots_count)
    parking_lots = []
    parking_lots_count.each do |count|
      parking_lots << ParkingLot.new(count)
    end
    parking_lots
  end
end