require_relative 'libraries'

class ControlPanel
  attr_accessor :parking_lot
  attr_reader :input, :input_path

  def execute
    file_name = ARGV.first
    if file_name
      assign_input_path(file_name)
      file_mode
    else
      interface_mode
    end
  end

  private

  def process_input
    options = input.chomp.split(' ')
    method = options.shift
    print_output(send(method, *options))
  rescue StandardError => _e
    print_output('Invalid Command')
  end

  def interface_mode
    while @input = STDIN.gets
      break if @input.chomp == 'exit'
      process_input
    end
  end

  def file_mode
    unless File.extname(input_path) == '.txt'
      return print_output(
        "Only .txt files accepted as input\n"
      )
    end
    input_file = File.open(input_path, 'r')
    input_file.each_line do |line|
      @input = line
      process_input
    end
  end

  def park(reg_number, color)
    if parking_lot.can_park?
      vehicle = Vehicle.new('car', color, reg_number)
      parking_slot = parking_lot.vacant_spots.sort_by(&:serial_number)[0]
      parking_slot.vehicle = vehicle
      "Allocated slot number: #{parking_slot.serial_number}\n"
    else
      "Sorry, parking lot is full\n"
    end
  end

  def leave(spot_id)
    vacated_spot = parking_lot.vacate_spot(spot_id.to_i)
    return "Slot number #{spot_id} does not exist\n" if vacated_spot.nil?
    "Slot number #{vacated_spot.serial_number} is free\n"
  end

  def status
    outputs = []
    print_output('Slot No.    Registration No    Colour')
    unless parking_lot.nil?
      parking_lot.occupied_spots.each do |spot|
        next unless spot
        output = spot.serial_number.to_s + '           ' +
                 spot.vehicle.reg_number + '      ' +
                 spot.vehicle.color
        outputs << output
      end
      outputs.each { |o| print_output(o) }
      outputs = []
    end
  end

  def registration_numbers_for_cars_with_colour(color)
    vehicles = parking_lot.find_vehicles_with_color(color)
    return "No cars found with #{color} color\n" if vehicles.empty?
    vehicles.map(&:reg_number).join(', ') + "\n"
  end

  def slot_numbers_for_cars_with_colour(color)
    spot_numbers = parking_lot.find_spots_with_color(color)
    return "No cars found with #{color} color\n" if spot_numbers.empty?
    spot_numbers.map(&:serial_number).join(', ') + "\n"
  end

  def slot_number_for_registration_number(reg_number)
    spot = parking_lot.find_spot_with_reg_number(reg_number)
    return "Not found\n" if spot.nil?
    "#{spot.serial_number}\n"
  end

  def create_parking_lot(spots)
    @parking_lot = ParkingLot.new(spots.to_i)
    parking_spots = parking_lot.spots
    return "Created a parking lot with #{parking_spots.size} slots\n" if parking_spots.any?
    "Count not create parking lot due to system errors\nexit\n"
  end

  def print_output(output)
    $stdout.puts output
  end

  def assign_input_path(file_path)
    @input_path = file_path
  end
end
