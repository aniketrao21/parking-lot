class ParkingSpot
  attr_reader :capacity, :row, :serial_number, :level
  attr_accessor :vehicle
  def initialize(vehicle, capacity, row, serial_number, level)
    @vehicle = vehicle
    @capacity = capacity
    @row = row
    @serial_number = serial_number
    @level = level
  end

  def vacant?
    vehicle.nil?
  end

  def occupied?
    !vehicle.nil?
  end
end
