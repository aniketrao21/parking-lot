require_relative 'constants'
class Vehicle
  attr_reader :type, :color, :reg_number, :size

  def initialize(type, color, reg_number)
    raise ArgumentError if type.nil? || color.nil? || reg_number.nil?
    @type = type.upcase
    @color = color.capitalize
    @reg_number = reg_number
    @size = Constants::VEHICLE_SIZES[@type]
  end
end
