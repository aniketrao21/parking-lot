module Constants
  VEHICLE_SIZES = {
    'BIKE' => 1,
    'CAR' => 2,
    'SEDAN' => 3,
    'SUV' => 4,
    'TRUCK' => 5,
    'BUS' => 6
  }.freeze
end
