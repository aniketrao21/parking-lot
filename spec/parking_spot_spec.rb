require 'spec_helper'

RSpec.describe ParkingSpot do
  it 'fails to initialize with no or nil attributes' do
    expect { ParkingSpot.new } .to raise_error(ArgumentError)
    expect { ParkingSpot.new(nil, nil, nil, nil) }.to raise_error(ArgumentError)
  end

  context 'it initializes and has attributes and functions' do
    before(:each) do
      @parking_spot = ParkingSpot.new(nil, 2, nil, 1, nil)
    end

    it 'should have capacity, serial number' do
      expect(@parking_spot.capacity).to eq 2

      expect(@parking_spot.serial_number).to eq 1
    end

    describe '#vacant?' do
      it 'should return true if vehicle is nil' do
        expect(@parking_spot.vacant?).to be_truthy
      end

      it 'should return false if vehicle is present' do
        @parking_spot.vehicle = Vehicle.new('car', 'white', 'MH-12-QT-9504')
        expect(@parking_spot.vacant?).to be_falsy
      end
    end

    describe '#occuiped?' do
      it 'should return false if vehicle is nil' do
        expect(@parking_spot.occupied?).to be_falsy
      end

      it 'should return true if vehicle is present' do
        @parking_spot.vehicle = Vehicle.new('car', 'white', 'MH-12-QT-9504')
        expect(@parking_spot.occupied?).to be_truthy
      end
    end
  end
end
