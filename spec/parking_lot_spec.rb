require 'spec_helper'
RSpec.describe ParkingLot do
  it 'fails to initialize with no or nil attributes' do
    expect { ParkingLot.new } .to raise_error(ArgumentError)
    expect { ParkingLot.new(nil) } .to raise_error(ArgumentError)
  end

  context 'it initializes and has attributes and functions' do
    before(:each) do
      @parking_lot = ParkingLot.new(10)
    end

    it 'should return a array of spot objects' do
      expect(@parking_lot.spots).not_to be_empty
      expect(@parking_lot.spots).to be_kind_of(Array)
      expect(@parking_lot.spots[0]).to be_kind_of(ParkingSpot)
    end

    describe '#can_park?' do
      it 'should return true if a vehicle can be parked' do
        expect(@parking_lot.can_park?).to be_truthy
      end

      it 'should return false if a vehicle can not be parked' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'black', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two
        expect(parking_lot.can_park?).to be_falsy
      end
    end

    describe '#vacant_spots' do
      it 'should return all spots that are vacant' do
        parking_lot = ParkingLot.new(2)
        vehicle = Vehicle.new('car', 'black', 'MH-12-FK-9504')
        parking_lot.spots.first.vehicle = vehicle

        expect(parking_lot.vacant_spots).to eq [parking_lot.spots.last]
      end
    end

    describe '#vacate_spot' do
      it 'should vacate a vehicle from occupied parking spot' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'black', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(parking_lot.vacate_spot(2)).to eq parking_lot.vacant_spots.last
        expect(parking_lot.vacant_spots.last.vehicle).to be_nil
      end

      it 'should return parking spot if spot is vacant' do
        parking_lot = ParkingLot.new(2)
        expect(parking_lot.vacate_spot(2)).to eq parking_lot.vacant_spots.last
      end

      it 'should return nil if spot does not exist' do
        parking_lot = ParkingLot.new(2)
        expect(parking_lot.vacate_spot(4)).to be_nil
      end
    end

    describe '#occupied_spots' do
      it 'should return an array of all occupied parking spots' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'black', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(parking_lot.occupied_spots).to eq parking_lot.spots
      end
    end

    describe '#find_vehicles_with_color' do
      it 'should return an array of all parked vehicles of given color' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'white', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(parking_lot.find_vehicles_with_color('White')).to eq [vehicle_one]
      end

      it 'should return an empty array no parked vehicles of given color are found' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'white', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(parking_lot.find_vehicles_with_color('Silver')).to eq []
      end
    end

    describe '#find_spot_with_reg_number' do
      it 'should return parked vehicle of given reg_number' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'white', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(
          parking_lot.find_spot_with_reg_number('MH-12-FK-9504')
        ).to eq parking_lot.spots.first
      end

      it 'should return nil if no parked vehicles of given reg_number are found' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'white', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(parking_lot.find_spot_with_reg_number('KA-01-TT-9929')).to be_nil
      end
    end

    describe '#find_spots_with_color' do
      it 'should return array of spots of given color' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'white', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(
          parking_lot.find_spots_with_color('White')
        ).to eq [parking_lot.spots.first]
      end

      it 'should return empty array of spots if vehicles of given color not found' do
        parking_lot = ParkingLot.new(2)
        vehicle_one = Vehicle.new('car', 'white', 'MH-12-FK-9504')
        vehicle_two = Vehicle.new('car', 'black', 'MH-12-QT-9504')
        parking_lot.spots.first.vehicle = vehicle_one
        parking_lot.spots.last.vehicle = vehicle_two

        expect(parking_lot.find_spots_with_color('Silver')).to be_empty
      end
    end
  end
end
