require 'spec_helper'

RSpec.describe ControlPanel, type: :aruba do
  it 'should not initialize with arguments' do
    expect { ControlPanel.new(nil) }.to raise_error ArgumentError
  end

  describe '#execute' do
    let(:expected) do
      <<-EOTXT
Created a parking lot with 6 slots
Allocated slot number: 1
Allocated slot number: 2
Allocated slot number: 3
Allocated slot number: 4
Allocated slot number: 5
Allocated slot number: 6
Slot number 4 is free
Slot No.    Registration No    Colour
1           KA-01-HH-1234      White
2           KA-01-HH-9999      White
3           KA-01-BB-0001      Black
5           KA-01-HH-2701      Blue
6           KA-01-HH-3141      Black
Allocated slot number: 4
Sorry, parking lot is full
KA-01-HH-1234, KA-01-HH-9999, KA-01-P-333
1, 2, 4
6
Not found
EOTXT
    end

    let(:invalid_expected) do
      <<-EOTXT
Slot No.    Registration No    Colour

Invalid Command
Invalid Command
Invalid Command
Invalid Command
EOTXT
    end

    let(:invalid_commands) do
      [
        "status\n",
        "leave 4\n",
        "registration_numbers_for_cars_with_colour White\n",
        "slot_numbers_for_cars_with_colour White\n",
        "slot_number_for_registration_number KA-01-HH-3141\n"
      ]
    end

    let(:commands) do
      [
        "create_parking_lot 6\n",
        "park KA-01-HH-1234 White\n",
        "park KA-01-HH-9999 White\n",
        "park KA-01-BB-0001 Black\n",
        "park KA-01-HH-7777 Red\n",
        "park KA-01-HH-2701 Blue\n",
        "park KA-01-HH-3141 Black\n",
        "leave 4\n",
        "status\n",
        "park KA-01-P-333 White\n",
        "park DL-12-AA-9999 White\n",
        "registration_numbers_for_cars_with_colour White\n",
        "slot_numbers_for_cars_with_colour White\n",
        "slot_number_for_registration_number KA-01-HH-3141\n",
        "slot_number_for_registration_number MH-04-AY-1111\n"
      ]
    end

    it 'should execute control panel methods as per inputs from file' do
      command = run("parking_lot
                    #{File.join(File.dirname(__FILE__),
                                '..',
                                'functional_spec',
                                'fixtures',
                                'file_input.txt')}")
      stop_all_commands
      expect(command.stdout).to eq(expected)
    end

    context 'in valid input scenario' do
      it 'should execute control panel methods as per given inputs' do
        command = run('parking_lot')
        commands.each { |cmd| command.write(cmd) }
        stop_all_commands
        expect(command.stdout).to eq(expected)
      end
    end

    context 'in an invalid input scenario' do
      it 'should execute control panel methods as per given inputs' do
        command = run('parking_lot')
        invalid_commands.each { |cmd| command.write(cmd) }
        stop_all_commands
        expect(command.stdout).to eq(invalid_expected)
      end

      it 'should only accept .txt files' do
        command = run("parking_lot
          #{File.join(File.dirname(__FILE__),
                      '..',
                      'spec',
                      'fixtures',
                      'sample.csv')}")
        stop_all_commands
        expect(command.stdout).to eq("Only .txt files accepted as input\n")
      end
    end
  end
end
