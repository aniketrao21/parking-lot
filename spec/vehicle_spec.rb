require 'spec_helper'

RSpec.describe Vehicle do
  it 'fails to initialize with no attributes' do
    expect { Vehicle.new } .to raise_error(ArgumentError)
    expect { Vehicle.new(nil, nil, nil) } .to raise_error(ArgumentError)
  end

  context 'initializes and has attributes and functions' do
    before(:each) do
      @vehicle = Vehicle.new('car', 'white', 'MH-12-QT-9504')
    end

    it 'should have a type, color, registration number, size' do
      expect(@vehicle.type).to eq 'CAR'
      expect(@vehicle.color).to eq 'White'
      expect(@vehicle.reg_number).to eq 'MH-12-QT-9504'
      expect(@vehicle.size).to eq 2
    end
  end
end
