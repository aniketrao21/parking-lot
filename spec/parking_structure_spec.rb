require 'spec_helper'
RSpec.describe ParkingStructure do

  context 'it initializes and has attributes and functions' do
    before(:each) do
      @parking_structure = ParkingStructure.new([2,3,4])
    end

    it 'should return a array of parking lot objects' do
      expect(@parking_structure.parking_lots).not_to be_empty
      expect(@parking_structure.parking_lots).to be_kind_of(Array)
      expect(@parking_structure.parking_lots.length).to eq 3
      expect(@parking_structure.parking_lots.first).to be_kind_of(ParkingLot)
      expect(@parking_structure.parking_lots.first.spots).to be_kind_of(Array)
      expect(@parking_structure.parking_lots.first.spots.length).to eq 2
      expect(@parking_structure.parking_lots.first.spots.first).to be_kind_of(ParkingSpot)
    end

  end
end
